package org.jordan.skyblock.engine;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.bukkit.scheduler.BukkitRunnable;
import org.jordan.skyblock.SkyBlock;
import org.jordan.skyblock.cmd.discord.CmdServer;
import org.jordan.skyblock.cmd.discord.DiscordCommand;
import org.jordan.skyblock.entity.Conf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class EngineDiscord extends ListenerAdapter {
    private List<DiscordCommand> commands = new ArrayList<>();

    public EngineDiscord() {
        commands.add(new CmdServer());
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        BukkitRunnable br = new BukkitRunnable() {
            @Override
            public void run() {
                if (event.getMessage().getContentRaw().isEmpty())return;
                if (Conf.get().getDiscordPrefix().isEmpty()){
                    SkyBlock.get().log("Error you need to have a prefix set in your MassiveSkyblock config");
                    return;
                }
                if (!event.getMessage().getContentRaw().startsWith(Conf.get().getDiscordPrefix()))return;
                List<String> args = new LinkedList<>(Arrays.asList(event.getMessage().getContentRaw().split(" ")));
                if (args.isEmpty())return;
                String base = args.get(0);
                args.remove(0);

                base = base.toLowerCase();
                base = base.split(Conf.get().getDiscordPrefix())[1];

                final String finished = base;

                commands.stream().filter(c -> c.getName().equalsIgnoreCase(finished)).forEach(c -> {
                    c.run(event.getChannel(),finished,event.getAuthor(),args);
                });
            }
        };
        br.runTaskAsynchronously(SkyBlock.get());
    }
}
