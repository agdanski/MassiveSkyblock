package org.jordan.skyblock.engine;

import com.massivecraft.massivecore.Engine;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.jordan.skyblock.entity.SPlayer;
import org.jordan.skyblock.entity.SPlayerColl;

public class EnginePlayerData extends Engine {
    private static EnginePlayerData i = new EnginePlayerData();
    public static EnginePlayerData get() {return i;}

    @EventHandler
    public void playerLoginEvent(PlayerJoinEvent event){
        if (SPlayer.get(event.getPlayer()) == null){
            SPlayerColl.get().create(event.getPlayer().getUniqueId().toString());
        }
    }
}
