package org.jordan.skyblock.engine;

import com.massivecraft.massivecore.Engine;
import com.massivecraft.massivecore.util.MUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.jordan.skyblock.entity.Conf;
import org.jordan.skyblock.entity.Island;
import org.jordan.skyblock.entity.SPlayer;
import org.jordan.skyblock.util.Board;
import org.jordan.skyblock.util.IslandUtil;
import org.jordan.skyblock.util.Loc;

public class EngineMovement extends Engine {
    private static EngineMovement i = new EngineMovement();
    public static EngineMovement get() {return i;}

    @EventHandler
    public void move(PlayerMoveEvent event){
        if (MUtil.isSameBlock(event))return;
        if (!event.getFrom().getWorld().getName().equalsIgnoreCase(Conf.get().getIslandWorld()))return;

        Player p = event.getPlayer();
        SPlayer sPlayer = SPlayer.get(p);

        if (sPlayer.isAdminMode())return;

        Loc loc = new Loc(event.getTo()).getChunk();

        if (Board.get().getIslandFromLoc(loc) == null)return;

        Island island = Board.get().getIslandFromLoc(loc);

        int x1 = island.getIsland().getFirst().getChunkX();
        int x2 = island.getIsland().getSecond().getChunkX();

        int z1 = island.getIsland().getFirst().getChunkZ();
        int z2 = island.getIsland().getSecond().getChunkZ();

        if (loc.getChunkX() >= x1 && loc.getChunkX() <= x2){
            if (loc.getChunkZ() >= z1 && loc.getChunkZ() <= z2){
                return;
            }
        }

        sPlayer.msg("&cYou cannot leave your island area.");
        event.setCancelled(true);
        event.setTo(event.getFrom());

    }

}
