package org.jordan.skyblock.entity;

import com.massivecraft.massivecore.MassiveCore;
import com.massivecraft.massivecore.store.Coll;

public class ConfColl extends Coll<Conf> {
    private static ConfColl i = new ConfColl();
    public static ConfColl get() {return i;}

    @Override
    public void onTick() {
        super.onTick();
    }

    @Override
    public void setActive(boolean active) {
        super.setActive(active);

        if (!active)return;

        Conf.i = this.get(MassiveCore.INSTANCE,true);

    }
}
