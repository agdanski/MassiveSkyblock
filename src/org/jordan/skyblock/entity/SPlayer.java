package org.jordan.skyblock.entity;

import com.massivecraft.massivecore.store.Entity;
import com.massivecraft.massivecore.store.SenderEntity;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jordan.skyblock.util.Board;
import org.jordan.skyblock.util.Loc;

import java.util.ArrayList;
import java.util.List;

public class SPlayer extends SenderEntity<SPlayer> {
    public static SPlayer get(Object oid){return SPlayerColl.get().get(oid);}

    ////////////////
    // RAW FIELDS //
    ////////////////

    private boolean adminMode = false;

    /////////////////
    //// METHODS ////
    ////////////////


    public String getIslandID(int a) {
        return islands.get(a);
    }

    public List<String> islands = new ArrayList<>();

    public boolean ownsIsland(){
        return !islands.isEmpty();
    }

    public List<String> getIslands() {
        return islands;
    }

    public boolean isAdminMode() {
        return adminMode;
    }

    public boolean isOnPlayersIsland(SPlayer other){
        if (isOffline())return false;
        Loc loc = new Loc(getPlayer().getLocation());
        if (Board.get().getIslandFromLoc(loc) == null)return false;
        Island island = Board.get().getIslandFromLoc(loc);

        return island.getOwner().equalsIgnoreCase(other.getId());
    }

    public boolean isOnIsland(Island is){
        if (isOffline())return false;
        Loc loc = new Loc(getPlayer().getLocation());
        if (Board.get().getIslandFromLoc(loc) == null)return false;
        Island island = Board.get().getIslandFromLoc(loc);
        return is.getId() == island.getId();
    }

    public void sendToSafePlace(){
        if (isOffline())return;
        if (ownsIsland()){
            getPlayer().teleport(Island.get(getIslandID(0)).getHome().asBukkitLocation());
            msg("&cYou have been kicked off this plot");
            return;
        }
        getPlayer().setHealth(10);
        msg("&cJordan is a lazy dev");
    }


    public void setAdminMode(boolean adminMode) {
        this.adminMode = adminMode;
    }
}
