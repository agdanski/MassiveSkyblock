package org.jordan.skyblock.entity;

import com.massivecraft.massivecore.store.SenderColl;

public class SPlayerColl extends SenderColl<SPlayer> {
    private static SPlayerColl i = new SPlayerColl();
    public static SPlayerColl get(){return i;}

    @Override
    public void onTick() {
        super.onTick();
    }

    @Override
    public void setActive(boolean active) {
        super.setActive(active);
    }
}
