package org.jordan.skyblock.entity;

import com.massivecraft.massivecore.Couple;
import com.massivecraft.massivecore.store.Entity;
import org.jordan.skyblock.cmd.IslandCommand;
import org.jordan.skyblock.util.Loc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Island extends Entity<Island> {
    public static Island get(String oid) {return IslandColl.get().get(oid);}

    private Long timeCreated;

    private Couple<Loc,Loc> island;

    private Couple<Loc,Loc> region;

    private Loc home;

    private String owner;

    private String locationIdentifier;

    private boolean open;

    private Set<String> members = new HashSet<>();
    private Set<String> bans = new HashSet<>();

    public String getLocationIdentifier() {
        return locationIdentifier;
    }

    public boolean isLocInsideIsland(Loc loc){
        Loc c = loc.getChunk();

        Loc chunk = island.getFirst();
        Loc chunk2 = island.getSecond();

        if (c.getChunkX() >= chunk.getChunkX() && c.getChunkX() <= chunk2.getChunkX()){
            return (c.getChunkZ() >= chunk.getChunkZ() && c.getChunkZ() <= chunk2.getChunkZ());
        }
        return false;
    }



    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setLocationIdentifier(String locationIdentifier) {
        this.locationIdentifier = locationIdentifier;
    }

    public String getOwner() {
        return owner;
    }

    public void create(int x,int z){
        int x1 = x * 16;
        int x2 = (x + 1) * 16;

        int z1 = z * 16;
        int z2 = (z + 1) * 16;

        region = new Couple<>(new Loc(x1,z1,Conf.get().getIslandWorld()),new Loc(x2,z2,Conf.get().getIslandWorld()));

        int x3 = x1 + 3;
        int x4 = x2 - 3;

        int z3 = z1 + 3;
        int z4 = z2 - 3;

        island = new Couple<>(new Loc(x3,z3,Conf.get().getIslandWorld()),new Loc(x4,z4,Conf.get().getIslandWorld()));

        timeCreated = System.currentTimeMillis();
    }

    public void setHome(Loc home) {
        this.home = home;
    }

    public Loc getHome() {
        return home;
    }

    public Long getTimeCreated() {
        return timeCreated;
    }

    public Couple<Loc, Loc> getIsland() {
        return island;
    }

    public Set<String> getBans() {
        return bans;
    }

    public Set<String> getMembers() {
        return members;
    }

    public Couple<Loc, Loc> getRegion() {
        return region;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }
}
