package org.jordan.skyblock.entity;

import com.massivecraft.massivecore.store.Coll;

public class IslandColl extends Coll<Island> {
    private static IslandColl i = new IslandColl();
    public static IslandColl get() {return i;}

    @Override
    public void setActive(boolean active) {
        super.setActive(active);
    }

    @Override
    public void onTick() {
        super.onTick();
    }
}
