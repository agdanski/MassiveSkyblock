package org.jordan.skyblock.entity;

import com.massivecraft.massivecore.store.Coll;
import com.massivecraft.massivecore.store.Entity;
import org.jordan.skyblock.SkyBlock;
import org.jordan.skyblock.util.Loc;

public class Conf extends Entity<Conf> {
    protected static Conf i;
    public static Conf get(){return i;}

    private Loc spawn;
    private String discordPrefix = "!";
    private String discordToken = "";
    private String schem = "island";
    private String islandWorld = "islands";
    private Integer startingHeight = 150;

    public String getIslandWorld() {
        return islandWorld;
    }

    public String getSchem(){
        return schem + ".schematic";
    }

    public Loc getSpawn() {
        return spawn;
    }

    public void setSpawn(Loc spawn) {
        this.spawn = spawn;
    }

    public Integer getStartingHeight() {
        return startingHeight;
    }

    public String getDiscordToken() {
        return discordToken;
    }

    public String getDiscordPrefix() {
        return discordPrefix;
    }
}
