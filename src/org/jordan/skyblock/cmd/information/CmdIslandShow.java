package org.jordan.skyblock.cmd.information;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.command.type.primitive.TypeInteger;
import com.massivecraft.massivecore.util.MUtil;
import org.jordan.skyblock.cmd.IslandCommand;
import org.jordan.skyblock.cmd.type.TypeSPlayer;
import org.jordan.skyblock.entity.Island;
import org.jordan.skyblock.entity.SPlayer;
import org.jordan.skyblock.util.IslandUtil;

import java.util.List;

public class CmdIslandShow  extends IslandCommand {
    public CmdIslandShow() {
        addParameter(TypeSPlayer.get(),"owner","you");
        addParameter(TypeInteger.get(),"island","number");
    }

    @Override
    public void perform() throws MassiveException {
        SPlayer found = readArg(sPlayer);
        Integer spot = readArg(1);

        int index = spot - 1;

        if (index < 0)index = 0;

        if (!found.ownsIsland()){
            msg("&cThis player does not own any islands");
            return;
        }

        if (found.getIslands().size() <= index){
            msg("&e" + found.getName() + " &7only has &e" + found.getIslands().size() + " &7islands");
            return;
        }
        Island island = Island.get(found.islands.get(index));

        IslandUtil.islandShow(sPlayer,island);
    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("show");
    }

    @Override
    public String getDesc() {
        return "view island information";
    }
}
