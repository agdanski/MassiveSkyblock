package org.jordan.skyblock.cmd.information;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.util.MUtil;
import org.jordan.skyblock.cmd.IslandCommand;

import java.util.List;

public class CmdIslandTop extends IslandCommand {
    @Override
    public void perform() throws MassiveException {
        super.perform();
    }

    @Override
    public String getDesc() {
        return "view top islands";
    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("top");
    }
}
