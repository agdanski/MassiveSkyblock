package org.jordan.skyblock.cmd.information;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.util.MUtil;
import org.jordan.skyblock.cmd.IslandCommand;

import java.util.List;

public class CmdIslandVersion extends IslandCommand {
    @Override
    public void perform() throws MassiveException {
        sPlayer.msg("&bMassiveSkyblock 1.0 &eby jordan");
    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("v","version");
    }

    @Override
    public String getDesc() {
        return "view version";
    }
}
