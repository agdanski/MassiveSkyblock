package org.jordan.skyblock.cmd.information;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.command.type.sender.TypePlayer;
import com.massivecraft.massivecore.util.MUtil;
import org.bukkit.entity.Player;
import org.jordan.skyblock.cmd.IslandCommand;
import org.jordan.skyblock.cmd.type.TypeSPlayer;
import org.jordan.skyblock.entity.SPlayer;
import org.jordan.skyblock.util.IslandUtil;

import java.util.List;

public class CmdIslandPlayer extends IslandCommand {
    public CmdIslandPlayer() {
        addParameter(TypeSPlayer.get(),"player","you");
    }

    @Override
    public void perform() throws MassiveException {
        if (senderIsConsole){
            msg("&cThis command is not compatible with console");
            return;
        }
        SPlayer found = readArg(sPlayer);

        IslandUtil.playerShow(this.sPlayer,found);
    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("player","p");
    }

    @Override
    public String getDesc() {
        return "view player info";
    }
}
