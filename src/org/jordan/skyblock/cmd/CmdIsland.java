package org.jordan.skyblock.cmd;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.util.MUtil;
import org.jordan.skyblock.cmd.information.CmdIslandPlayer;
import org.jordan.skyblock.cmd.information.CmdIslandShow;
import org.jordan.skyblock.cmd.information.CmdIslandTop;
import org.jordan.skyblock.cmd.information.CmdIslandVersion;
import org.jordan.skyblock.cmd.management.*;
import org.jordan.skyblock.cmd.misc.CmdIslandAdmin;
import org.jordan.skyblock.cmd.misc.CmdIslandDev;
import org.jordan.skyblock.cmd.misc.CmdIslandHome;

import java.util.List;

public class CmdIsland extends IslandCommand {
    private static CmdIsland i = new CmdIsland();
    public static CmdIsland get(){return i;}

    private CmdIslandAccess access = new CmdIslandAccess();
    private CmdIslandBan ban = new CmdIslandBan();
    private CmdIslandEvict evict = new CmdIslandEvict();
    private CmdIslandKick kick = new CmdIslandKick();
    private CmdIslandInfo info = new CmdIslandInfo();
    private CmdIslandOpen open = new CmdIslandOpen();
    private CmdIslandSethome sethome = new CmdIslandSethome();

    private CmdIslandCreate create = new CmdIslandCreate();
    private CmdIslandHome home = new CmdIslandHome();
    private CmdIslandTop top = new CmdIslandTop();
    private CmdIslandPlayer player = new CmdIslandPlayer();
    private CmdIslandShow show = new CmdIslandShow();
    private CmdIslandVersion version = new CmdIslandVersion();
    private CmdIslandDev dev = new CmdIslandDev();
    private CmdIslandAdmin admin = new CmdIslandAdmin();


    public CmdIsland() {
        addChild(access);
        addChild(ban);
        addChild(evict);
        addChild(kick);
        addChild(create);
        addChild(home);
        addChild(top);
        addChild(player);
        addChild(show);
        addChild(admin);
        addChild(dev);
        addChild(version);
        addChild(info);
        addChild(sethome);
        addChild(open);
    }

    @Override
    public void perform() throws MassiveException {
        super.perform();
    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("island","i","is");
    }

    @Override
    public String getDesc() {
        return "Island Base Command";
    }
}
