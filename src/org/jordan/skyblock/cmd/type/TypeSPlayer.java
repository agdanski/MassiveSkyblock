package org.jordan.skyblock.cmd.type;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.command.type.Type;
import com.massivecraft.massivecore.command.type.TypeAbstract;
import org.bukkit.command.CommandSender;
import org.jordan.skyblock.entity.SPlayer;
import org.jordan.skyblock.entity.SPlayerColl;

import java.util.Collection;

public class TypeSPlayer {
    public static Type<SPlayer> get() {return SPlayerColl.get().getTypeEntity();}
}
