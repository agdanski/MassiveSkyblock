package org.jordan.skyblock.cmd.management;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.util.MUtil;
import org.jordan.skyblock.cmd.IslandCommand;

import java.util.List;

public class CmdIslandBan extends IslandCommand {

    @Override
    public void perform() throws MassiveException {
        super.perform();
    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("ban");
    }

    @Override
    public String getDesc() {
        return "ban players from your island";
    }
}
