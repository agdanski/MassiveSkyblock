package org.jordan.skyblock.cmd.management;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.util.MUtil;
import org.jordan.skyblock.cmd.IslandCommand;
import org.jordan.skyblock.cmd.type.TypeSPlayer;

import java.util.List;

public class CmdIslandKick extends IslandCommand {

    public CmdIslandKick() {
        addParameter(TypeSPlayer.get(),"player");
    }

    @Override
    public void perform() throws MassiveException {

    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("kick");
    }

    @Override
    public String getDesc() {
        return "unaccess players from your plot";
    }
}
