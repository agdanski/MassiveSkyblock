package org.jordan.skyblock.cmd.management;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.util.MUtil;
import org.jordan.skyblock.cmd.IslandCommand;

import java.util.List;

public class CmdIslandOpen extends IslandCommand {
    @Override
    public void perform() throws MassiveException {
        if (!isOnOwnedIsland()){
            return;
        }
        boolean ret = !getIsland().isOpen();
        String parsed = ret ? "&aOPEN" : "&cCLOSED";
        getIsland().setOpen(ret);

        msg("&bYour island is now " + parsed);
    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("open");
    }

    @Override
    public String getDesc() {
        return "make island public to visitors";
    }
}
