package org.jordan.skyblock.cmd.management;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.util.MUtil;
import org.bukkit.Bukkit;
import org.jordan.skyblock.cmd.IslandCommand;
import org.jordan.skyblock.entity.Conf;
import org.jordan.skyblock.entity.Island;
import org.jordan.skyblock.util.Board;
import org.jordan.skyblock.util.IslandUtil;

import java.util.List;

public class CmdIslandInfo extends IslandCommand {

    @Override
    public void perform() throws MassiveException {
        if (!loc.getWorld().equalsIgnoreCase(Conf.get().getIslandWorld())){
            msg("&cYou are not in the island world currently");
            return;
        }

        if (Board.get().getIslandFromLoc(loc) == null){
            msg("&cError!");
            return;
        }

        Island island = Board.get().getIslandFromLoc(loc);
        IslandUtil.islandShow(sPlayer,island);
    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("info");
    }

    @Override
    public String getDesc() {
        return "view info on island you are in";
    }
    public boolean equals(int a,int b){
        return a == b;
    }
}
