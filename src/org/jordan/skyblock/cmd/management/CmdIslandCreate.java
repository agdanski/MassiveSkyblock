package org.jordan.skyblock.cmd.management;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.util.MUtil;
import org.jordan.skyblock.cmd.IslandCommand;
import org.jordan.skyblock.util.IslandUtil;

import java.util.List;

public class CmdIslandCreate extends IslandCommand {
    @Override
    public void perform() throws MassiveException {
        if (sPlayer.ownsIsland() && !sPlayer.getPlayer().isOp()){
            msg("&cYou already have an island!");
            return;
        }
        if (IslandUtil.createIsland(sPlayer)){
            msg("&aIsland created successfully! Use /is home");
            return;
        }
        msg("&cThere was an issue creating your island");
    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("create");
    }

    @Override
    public String getDesc() {
        return "create an island";
    }
}
