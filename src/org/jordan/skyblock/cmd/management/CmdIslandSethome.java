package org.jordan.skyblock.cmd.management;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.util.MUtil;
import org.jordan.skyblock.cmd.IslandCommand;
import org.jordan.skyblock.util.Loc;

import java.util.List;

public class CmdIslandSethome extends IslandCommand {
    @Override
    public void perform() throws MassiveException {
        if (!isOnOwnedIsland())return;
        getIsland().setHome(new Loc(sPlayer.getPlayer().getLocation()));
        msg("&eHome &bhas been set for your island");
    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("sethome");
    }

    @Override
    public String getDesc() {
        return "set your island home location";
    }
}
