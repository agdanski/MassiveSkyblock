package org.jordan.skyblock.cmd.management;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.command.type.primitive.TypeInteger;
import com.massivecraft.massivecore.util.MUtil;
import org.jordan.skyblock.cmd.IslandCommand;
import org.jordan.skyblock.cmd.type.TypeSPlayer;
import org.jordan.skyblock.entity.Island;
import org.jordan.skyblock.entity.SPlayer;
import org.jordan.skyblock.util.Board;
import org.jordan.skyblock.util.Loc;

import java.util.List;

public class CmdIslandAccess extends IslandCommand {

    public CmdIslandAccess() {
        addParameter(TypeSPlayer.get(),"player");
    }

    @Override
    public void perform() throws MassiveException {

        if(!isOnOwnedIsland()){
            return;
        }

        Island island = Board.get().getIslandFromLoc(new Loc(sPlayer.getPlayer().getLocation()));

        SPlayer found = readArg();

        if (found.getId() == sPlayer.getId()){
            msg("&cYou cannot access yourself to your island");
            return;
        }

        if (island.getMembers().contains(found.getId())){
            msg("&e" + found.getName() +" &bis already accessed to this island");
            return;
        }

        island.getMembers().add(found.getId());
        msg("&bAccessed &e" + found.getName() + " &bto your island");
    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("access");
    }

    @Override
    public String getDesc() {
        return "access players to your island";
    }
}
