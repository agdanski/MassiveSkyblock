package org.jordan.skyblock.cmd.management;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.util.MUtil;
import org.jordan.skyblock.cmd.IslandCommand;
import org.jordan.skyblock.cmd.type.TypeSPlayer;
import org.jordan.skyblock.entity.SPlayer;

import java.util.List;

public class CmdIslandEvict extends IslandCommand {

    public CmdIslandEvict() {
        addParameter(TypeSPlayer.get(),"player");
    }

    @Override
    public void perform() throws MassiveException {
        if (!isOnOwnedIsland()){
            return;
        }
        SPlayer found = readArg();
        if (found.isOffline()){
            msg("&dSorry! &bthat player is offline!");
            return;
        }

        if (found.isOnIsland(getIsland())){
            msg("&e" + found.getName() +" &bwas kicked off your island");
            found.sendToSafePlace();
            return;
        }
        msg("&cThat player was not on this island");
    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("evict");
    }

    @Override
    public String getDesc() {
        return "remove players off your island";
    }
}
