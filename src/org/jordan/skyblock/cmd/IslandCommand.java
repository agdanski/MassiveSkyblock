package org.jordan.skyblock.cmd;

import com.massivecraft.massivecore.command.MassiveCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jordan.skyblock.entity.Island;
import org.jordan.skyblock.entity.SPlayer;
import org.jordan.skyblock.util.Board;
import org.jordan.skyblock.util.Loc;

public class IslandCommand extends MassiveCommand {
    public SPlayer sPlayer;
    public Loc loc;

    @Override
    public void senderFields(boolean set) {
        this.sPlayer = set ? SPlayer.get(sender) : null;
        this.loc = set ? new Loc(sPlayer.getPlayer().getLocation()) : null;
    }

    protected boolean isOnOwnedIsland(){
        Island island = Board.get().getIslandFromLoc(loc);
        if (island == null){
            msg("&cYou are currently not on an island");
            return false;
        }
        if (!island.getOwner().equalsIgnoreCase(sPlayer.getId())){
            msg("&cYou do not own this island");
            return false;
        }
        return true;
    }

    protected Island getIsland(){
        return Board.get().getIslandFromLoc(loc);
    }
}
