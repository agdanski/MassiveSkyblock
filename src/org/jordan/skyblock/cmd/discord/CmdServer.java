package org.jordan.skyblock.cmd.discord;

import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import java.util.List;

public class CmdServer extends DiscordCommand {
    @Override
    public void run(MessageChannel ch, String base, User auth, List<String> args) {
        ch.sendMessage("Message recieved! This message was sent by " + auth.getAsMention() + " and had a base command of " + base +" and had " + args.size() + " arguments").queue();
    }

    @Override
    public String getName() {
        return "server";
    }
}
