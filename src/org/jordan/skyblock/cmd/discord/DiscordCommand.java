package org.jordan.skyblock.cmd.discord;

import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import java.util.List;

public abstract class DiscordCommand {
    public abstract void run(MessageChannel ch, String base, User auth,List<String> args);
    public abstract String getName();
}
