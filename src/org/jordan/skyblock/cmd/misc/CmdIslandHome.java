package org.jordan.skyblock.cmd.misc;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.command.type.primitive.TypeInteger;
import com.massivecraft.massivecore.util.MUtil;
import org.jordan.skyblock.cmd.IslandCommand;
import org.jordan.skyblock.entity.Island;

import java.util.List;

public class CmdIslandHome extends IslandCommand {

    public CmdIslandHome() {
        addParameter(TypeInteger.get(),"number","1");
    }

    @Override
    public void perform() throws MassiveException {
        int found = readArg(1);
        int index = found - 1;
        if (index < 0)index = 0;

        if (!sPlayer.ownsIsland()){
            msg("&cYou own no islands! &bUse &e/is create");
            return;
        }

        if (sPlayer.getIslands().size() <= index){
            msg("&eYou &7only have &e" + sPlayer.getIslands().size() + " &7islands");
            return;
        }

        Island island = Island.get(sPlayer.getIslandID(index));
        sPlayer.getPlayer().teleport(island.getHome().asBukkitLocation());
        msg("&bTeleporting!");
    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("home");
    }

    @Override
    public String getDesc() {
        return "go to island";
    }
}
