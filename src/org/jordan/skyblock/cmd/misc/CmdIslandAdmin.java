package org.jordan.skyblock.cmd.misc;


import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.util.MUtil;
import org.jordan.skyblock.cmd.IslandCommand;

import java.util.List;

public class CmdIslandAdmin extends IslandCommand {
    @Override
    public void perform() throws MassiveException {
        if (!sender.hasPermission("island.admin") && !sender.isOp()){
            msg("&cNo Permission!");
            return;
        }

        boolean ret = !sPlayer.isAdminMode();

        sPlayer.setAdminMode(ret);

        String color = ret ? "&aON" : "&cOFF";

        msg("&bYou have set &d&lADMIN MODE &eto " + color);

    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("admin");
    }

    @Override
    public String getDesc() {
        return "bypass restrictions";
    }
}
