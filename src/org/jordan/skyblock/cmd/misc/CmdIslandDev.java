package org.jordan.skyblock.cmd.misc;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.util.MUtil;
import org.jordan.skyblock.SkyBlock;
import org.jordan.skyblock.cmd.IslandCommand;

import java.io.File;
import java.util.List;

public class CmdIslandDev extends IslandCommand {
    @Override
    public void perform() throws MassiveException {
        String d = File.separator;
        msg("plugins" + d +"WorldEdit" + d +"schematics" + d + "island.schematic");
    }

    @Override
    public List<String> getAliases() {
        return MUtil.list("dev");
    }

    @Override
    public String getDesc() {
        return "for dev use only!";
    }
}
