package org.jordan.skyblock;

import com.massivecraft.massivecore.MassivePlugin;
import org.bukkit.Bukkit;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.jordan.skyblock.entity.Conf;
import org.jordan.skyblock.entity.Island;
import org.jordan.skyblock.entity.IslandColl;
import org.jordan.skyblock.util.Board;
import org.jordan.skyblock.util.Discord;
import org.jordan.skyblock.util.Loc;

public class SkyBlock extends MassivePlugin {

    private static SkyBlock i;
    public static SkyBlock get(){return i;}

    public SkyBlock() {
        SkyBlock.i = this;
    }

    public Discord discord;

    @Override
    public void onEnableInner() {
        super.onEnableInner();
        activateAuto();

        for (Island island : IslandColl.get().getAll()){
            Board.get().getIslands().put(island.getLocationIdentifier(),island.getId());
        }

        if (Conf.get().getSpawn() == null){
            generateSpawnLocation();
        }
        discord = new Discord();
        discord.run();
    }

    @Override
    public void onDisable() {
        discord.discord.shutdownNow();
        discord.stop();
        super.onDisable();
    }

    public Discord getDiscord() {
        return discord;
    }

    private void generateSpawnLocation(){
        Conf.get().setSpawn(new Loc(Bukkit.getWorlds().get(0).getBlockAt(100,100,100).getLocation()));
    }


}
