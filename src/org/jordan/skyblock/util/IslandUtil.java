package org.jordan.skyblock.util;

import com.massivecraft.massivecore.store.MStore;
import com.massivecraft.massivecore.util.IdUtil;
import com.massivecraft.massivecore.util.MUtil;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.MCEditSchematicFormat;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.jordan.skyblock.SkyBlock;
import org.jordan.skyblock.entity.Conf;
import org.jordan.skyblock.entity.Island;
import org.jordan.skyblock.entity.IslandColl;
import org.jordan.skyblock.entity.SPlayer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class IslandUtil {
    public static boolean createIsland(SPlayer sPlayer){

        int f = ((int)Math.sqrt(IslandColl.get().getAll().size())) + 1;

        SkyBlock.get().log(f);

        for (int x = 0; x < f; x++){
            for (int z = 0; z < f;z++){
                if (Board.get().isSpotTaken(x,z)){
                    continue;
                }
                applyNewIsland(x,z,sPlayer);
                return true;
            }
        }
        return false;
    }

    private static void applyNewIsland(int x,int z,SPlayer sPlayer){
        String id = MStore.createId();

        Island island = IslandColl.get().create(id);
        island.setOwner(sPlayer.getId());
        island.setLocationIdentifier(Board.get().idFromInts(x,z));

        sPlayer.getIslands().add(id);

        Board.get().getIslands().put(island.getLocationIdentifier(),island.getId());

        //handle chunks
        island.create(x,z);
        // paste
        Loc first = island.getIsland().getFirst();
        Loc second = island.getIsland().getSecond();

        int corner = first.getChunkX() * 16;
        int corner2 = first.getChunkZ() * 16;
        int y = Conf.get().getStartingHeight();

        int middleX = ((second.getChunkX() * 16) + corner) / 2;
        int middleZ = ((second.getChunkZ() * 16) + corner2) / 2;

        island.setHome(new Loc(new Location(Bukkit.getWorld(Conf.get().getIslandWorld()),middleX,y,middleZ)));

        Location location = new Location(Bukkit.getWorld(Conf.get().getIslandWorld()),middleX,y,middleZ);
        WorldEditPlugin plugin = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
        String d = File.separator;
        File schematic = new File("plugins" + d +"WorldEdit" + d +"schematics" + d + Conf.get().getSchem());
        EditSession session = plugin.getWorldEdit().getEditSessionFactory().getEditSession(new BukkitWorld(location.getWorld()),100000000);
        try {
            CuboidClipboard clipboard = MCEditSchematicFormat.getFormat(schematic).load(schematic);
            clipboard.paste(session,new Vector(location.getX(),location.getY(),location.getZ()),false);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DataException e) {
            e.printStackTrace();
        } catch (MaxChangedBlocksException e) {
            e.printStackTrace();
        }

    }

    public static void playerShow(SPlayer inform, SPlayer about){
        BukkitRunnable br = new BukkitRunnable() {
            @Override
            public void run() {
                List<String> lines = new ArrayList<>();
                lines.add("&7&m--------- &d&l"+about.getName()+"&7&m---------");
                lines.add("&bIsland Count &d&l" + about.getIslands().size());
                lines.add("&badd more stuff here");
                inform.msg(lines);
            }
        };
        br.runTaskAsynchronously(SkyBlock.get());
    }

    public static void islandShow(SPlayer sPlayer, Island island){
        BukkitRunnable br = new BukkitRunnable() {
            @Override
            public void run() {
                List<String> lines = new ArrayList<>();
                lines.add("&7&m-----&d&l " + sPlayer.getName() +"'s Island &7&m-----");
                lines.add("&bCreated: &e" + TimeUtil.formatDurationWords(System.currentTimeMillis() - island.getTimeCreated(),false,false,false));

                String parsed = island.isOpen() ? "&aOPEN" : "&cCLOSED";

                lines.add("&bStatus: " + parsed);
                lines.add("&bWorth: &a$100,000");
                lines.add("&bMembers:");
                String members = "&e";
                for (String member : island.getMembers()){
                  members += SPlayer.get(member).getName() + ", ";
                }
                if (island.getMembers().isEmpty()){
                    members = "&eNone!";
                }
                lines.add(members);
                sPlayer.msg(lines);
            }
        };
        br.runTaskAsynchronously(SkyBlock.get());
    }

}
