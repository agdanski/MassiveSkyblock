package org.jordan.skyblock.util;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.block.Block;

public class Loc {
    private Integer blockX;
    private Integer blockY;
    private Integer blockZ;

    private Double locationX;
    private Double locationY;
    private Double locationZ;
    private Float pitch;
    private Float yaw;

    private Integer chunkX;
    private Integer chunkZ;

    private Boolean isTypeChunk = false;
    private Boolean isTypeBlock = false;
    private Boolean isTypeLoc = false;


    private String world;

    // Constructors

    public Loc(Block b) {
        blockX = b.getX();
        blockY = b.getY();
        blockZ = b.getZ();
        world = b.getWorld().getName();
        isTypeBlock = true;
    }

    public Loc(Location l){
        locationX = l.getX();
        locationY = l.getY();
        locationZ = l.getZ();
        pitch = l.getPitch();
        yaw = l.getYaw();
        world = l.getWorld().getName();
        isTypeLoc = true;
    }

    public Loc(Chunk c){
        chunkX = c.getX();
        chunkZ = c.getZ();
        world = c.getWorld().getName();
        isTypeChunk = true;
    }

    public Loc(int x,int z,String world){
        chunkX = x;
        chunkZ = z;
        this.world = world;
        isTypeChunk = true;
    }

    // Methods


    public Loc getChunk(){
        return new Loc(asBukkitChunk());
    }

    public Chunk asBukkitChunk(){
        if (isTypeChunk){
            return Bukkit.getWorld(world).getChunkAt(chunkX,chunkZ);
        }
        return Bukkit.getWorld(world).getChunkAt(asBukkitLocation());
    }

    public Location asBukkitLocation(){
        if (isTypeLoc){
            return new Location(Bukkit.getWorld(world),locationX,locationY,locationZ,pitch,yaw);
        }
        if (!isTypeBlock) return null;
        return new Location(Bukkit.getWorld(world),blockX,blockY,blockZ);
    }

    public Block asBukkitBlock(){
        return asBukkitLocation().getBlock();
    }

    public int getChunkX() {
        return chunkX;
    }

    public int getChunkZ() {
        return chunkZ;
    }

    public String getWorld() {
        return world;
    }
}
