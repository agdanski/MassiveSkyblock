package org.jordan.skyblock.util;

import org.jordan.skyblock.entity.Conf;
import org.jordan.skyblock.entity.Island;

import java.util.HashMap;
import java.util.Map;

public class Board {
    private static Board i = new Board();
    public static Board get() {return i;}

    private Map<String, String> islands = new HashMap<>();

    public Map<String, String> getIslands() {
        return islands;
    }

    public Island getIslandFromLoc(Loc loc){
        if (!loc.getWorld().equalsIgnoreCase(Conf.get().getIslandWorld())){
            return null;
        }
        Loc c = loc.getChunk();
        int first = c.getChunkX() / 16;
        int second = c.getChunkZ() / 16;

        String id = idFromInts(first,second);

        return Island.get(islands.getOrDefault(id,"empty"));

    }

    public String idFromInts(int x,int z){
        return x + ":" + z;
    }

    public boolean isSpotTaken(int x, int z){
        return islands.containsKey(idFromInts(x,z));
    }




}
