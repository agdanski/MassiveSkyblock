package org.jordan.skyblock.util;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import org.jordan.skyblock.engine.EngineDiscord;
import org.jordan.skyblock.entity.Conf;

import javax.security.auth.login.LoginException;

public class Discord extends Thread {
    public JDA discord;
    @Override
    public void run() {
        super.run();
        try {
            discord = new JDABuilder(AccountType.BOT).setToken(Conf.get().getDiscordToken()).build();
            discord.addEventListener(new EngineDiscord());
        } catch (LoginException e) {
            e.printStackTrace();
        }
    }
    public JDA getDiscord() {
        return discord;
    }
}
