package org.jordan.skyblock;

public enum Perm {
    BASECOMMAND,
    CREATE,
    HOME,
    TOP,
    VERSION
}
